import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green';

const theme = createMuiTheme({
  palette: {
      primary: blue,
      action: {
	  active: green
      }
  },
    overrides: {
	MuiInput: {
	    underline: {
		'&:after': {
      borderBottom: `2px solid #FFFFFF`,
    },
    '&$focused:after': {
      borderBottomColor: `#FFFFFF`,
    },
    '&$error:after': {
      borderBottomColor: `#FFFFFF`,
    },
    '&:before': {
      borderBottom: ` #FFFFFF`,
    },
    '&:hover:not($disabled):not($focused):not($error):before': {
      borderBottom: ` #FFFFFF`,
    },
    '&$disabled:before': {
      borderBottom: ' #FFFFFF',
	    },
	},
	},
    },
});

export default theme;
