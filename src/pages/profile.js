import React from 'react';
import { Link, Route } from 'react-router-dom';
import NavButton from '../components/navbutton.js';
import TimeLine from '../components/timeline.js';
import NavBar from '../components/navbar.js';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import Grid from '@material-ui/core/Grid';
import TemporaryDrawer from '../components/sidemenu.js'
import ReactDOM from "react-dom";

import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import TimelineEvent from "../components/timeline-event.js";



const styles = {
    button: {
	borderRadius: 2,
	width: 150,
	height: 50,
	color: 'white',
	background: '#013243',
	'&:hover': {
            color: '#013243',
            background: '#F6F6F6',
	}
    },

    formcontrol: {
	width: 500,
	margin: 20,
    },

    textfield: {
	background: 'white',
	padding: 5,
	border: '1px solid lightgrey',
	borderRadius: 3,
    },

    input: {
	color:"#013243",
	border: 'none',
	paddingLeft: 5,
    },

    h3: {
	fontSize: 16,
	padding: 0,
	margin: 0,
	lineHeight: 0,
	color: '#013243',
	textAlign: 'left',
    },

    h2: {
	color: '#013243',
    },

    posth2: {
	color: 'white',
    },

    profilecard: {
	background: '#F6F6F6',
	width: '90em',
	padding: 25,
	margin: 'auto',
	marginTop: '7em',
	borderRadius: 3,
	height: 'auto',
	display: 'inline-block'
	

    },

   infocard: {
},

    linktologin: {
	textDecoration: 'none',
	color: '#013243',
	'&:hover': {
            textDecoration: 'underline',
	}
    },

    
    

};

class ProfilePage extends React.Component{
    componentDidMount() {
      window.scrollTo(0, 0)
    }   
   constructor(props){
    super(props);
    this.state = {
            email: '',
            username: '',
            city: '',
            firstname: '',
            lastname: '',
        school: '',
        events: [

        {
            title: 'Freshman',
            desc: 'First Year of HS'
        },


        {
            title: 'Doctor',
            desc: 'Work in the med field'
        },
        

    ],

    recommendations: [
        
    ],
	
	savedata: []

        };
    }
   componentDidMount() {

      window.scrollTo(0, 0);
      let uid = this.props.auth.uid
      this.props.firebase.ref(`/users/${uid}/timeline`).once('value').then(function(snapshot){
        console.log(snapshot);
      });

     
    } 

    addEvent(num){
        console.log(num);
        let uid = this.props.auth.uid;
        let previous_recommendations = [];
        if(this.props.profile.timeline){
            previous_recommendations = this.props.profile.timeline.recommendation;
        }
        let events = [...previous_recommendations, this.state.recommendations[num]];
        
        this.props.firebase.update(`/users/${uid}/timeline`,
            {
                recommendation: events,
                
            });
    }


    getData = (val) => {
        let previous_recommendations = [];
        let temp = {
            
            title: val,
            desc: 'This recommendation has been generated in back-end'
        
        };
        let events = [...this.state.recommendations, temp];
        this.setState({recommendations: events});
        
    }
    handleChange(event, field){
        this.setState({
            [field]: event.target.value
        });
    }

    logout(){
	this.props.firebase.logout();
    }

    render(){
	            let tempEvents = [];
            if(isEmpty(this.props.profile) || isEmpty(this.props.profile.timeline)){
                // do nothing
            }else{
                tempEvents = this.props.profile.timeline.recommendation;
            }
            console.log(this.props.profile);																																															
	
	return(
	<div>

    	<div>
    	    <header>
				<p id="title"><a href="/HomePage" style={{float:"left", marginLeft:400, fontSize:45, fontWeight: "bold"}}>My Career Timeline</a>

				
			</p>
			
			<p id="title">
				<Link to="/HomePage"><button variant="contained"
			    color="secondary" 
				style={{float:"left", fontWeight:"bold", fontSize:20, padding:"0 0 0 0"}}
			    onClick={() => {this.props.firebase.logout();}}>
			Logout
		    </button></Link>
		    	<AnchorLink href='#card1'>Demo</AnchorLink>
		    	</p>
				
	    	</header>
    	</div>

		<div>

	
		<br/>
		<div className={this.props.classes.profilecard}>
			<div className={this.props.classes.infocard}>
			<img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" height ="200" />  
    			<h1 className={this.props.classes.h2}>{this.props.profile.firstname} {this.props.profile.lastname}</h1>
    			<p>Goes to {this.props.profile.school}</p>

    			<p><TemporaryDrawer callbackFunction = {this.getData} /></p>


            </div>


<br/>
<br/>


<font color="#013243" size="25 "> <b> My Timeline </b> </font>


		 <TimelineEvent array = {tempEvents} />
                          {this.state.recommendations.map((item, index) => (
                        <button className="timeButton" onClick={this.addEvent.bind(this,index)}>
                            <div className="entry">
                                <h1>{item.title}</h1>
                                {item.desc}
                            </div>
                        </button>
                        ))}

	    </div>
		</div>
</div>

	    
	)
    }
};


 export default withStyles(styles)(compose(
    firebaseConnect(),
    firebaseConnect((props) => [
    { path: 'recommendation' }
  ]),
 
    connect((state, props) => ({events: state.firebase.data.events})),
    connect(({firebase: {auth}}) => ({auth})),
    connect(({firebase: { profile } }) => ({ profile }))
)
(ProfilePage));