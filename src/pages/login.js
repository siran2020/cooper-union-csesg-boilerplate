import React from 'react';
import { Link } from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom'
import AnchorLink from 'react-anchor-link-smooth-scroll';
import Grid from '@material-ui/core/Grid';

const styles = {
  button: {
    borderRadius: 2,
    width: 150,
    height: 50,
    color: 'white',
    background: '#013243',
    '&:hover': {
        color: '#013243',
        background: '#F6F6F6',
    }
  },

  formcontrol: {
    width: 500,
    margin: 20,
  },

  textfield: {
    background: 'white',
    padding: 5,
    border: '1px solid lightgrey',
    borderRadius: 3,
},

  input: {
    color:"#013243",
    border: 'none',
    paddingLeft: 5,
},

  h3: {
    fontSize: 16,
    padding: 0,
    margin: 0,
    lineHeight: 0,
    color: '#013243',
    textAlign: 'left',
},

  h2: {
    color: '#013243',
},

  posth2: {
    color: 'white',
},

  signupcard: {
    background: '#F6F6F6',
    width: '35em',
    padding: 25,
    margin: 'auto',
    marginTop: '3em',
    borderRadius: 3

},

  linktologin: {
    textDecoration: 'none',
    color: '#013243',
    '&:hover': {
        textDecoration: 'underline',
    }
},

 
 

};



class LoginPage extends React.Component{
componentDidMount() {
  window.scrollTo(0, 0)
}
    constructor(props){
       super(props);
       this.state = {
        email: '',
        password: '',
        username: '',
        city: '',
        firstname: '',
        lastname: ''
    };    
    }

    handleChange(event, field){
        this.setState({
            [field]: event.target.value
        });
        }
    
    handleSubmit(event){
        event.preventDefault();
        this.props.firebase.login(this.state)
           .then((response) => {
            this.props.history.push('/profile')
            
            })
            .catch((error) => {
            switch(error.code){
                case 'auth/user-not-found':
                alert('It seems like you dont have an account? Create one now!')
                break;
                case 'auth/wrong-password':
                alert('Uh oh! Looks like you put the wrong password.')
                case 'auth/invalid-email':
                alert('That email isnt valid')
                break;
                case 'auth/network-request-failed':
                alert('Something wrong with your network.')
                break;
                default:
                // default error
            }
            });
        }

logout(){
    this.props.firebase.logout();
    }


       render(){
console.log(this.props.profile);
    let payload;
    if(!this.props.auth.isLoaded){
        // auth is not warmed up
        payload = null;
		
		
		
    }
    if(this.props.auth.isLoaded && this.props.auth.isEmpty){
        // auth is ready
        // but user is not logged in
        payload = <form onSubmit={(event) => {this.handleSubmit(event);}}>
        <div className={this.props.classes.signupcard}>
        <h2
        className={this.props.classes.h2}>Login!</h2> 
        <FormControl
            className={this.props.classes.formcontrol}>
            <h3
            className={this.props.classes.h3}
            >Email:</h3>
            <TextField
            className={this.props.classes.textfield}
            InputProps={{
                className: this.props.classes.input,
            }}
            value={this.state.email}
            onChange={(event) => {this.handleChange(event, 'email');}}
            margin="normal"
            id="required"
            />
        </FormControl>
        <FormControl
            className={this.props.classes.formcontrol}>
            <h3
            className={this.props.classes.h3}
            >Password:</h3>
            <TextField
            className={this.props.classes.textfield}
            InputProps={{
                className: this.props.classes.input,
            }}
            type="password"
            value={this.state.password}
            onChange={(event) => {this.handleChange(event, 'password');}}
            margin="normal"
            id="required"
            />
        </FormControl>
        <Button
            type="submit"
            className={this.props.classes.button}>
            Login
        </Button>
        <br/>
        <p><a href="/signup"
        className={this.props.classes.linktologin}
        >Dont have an account yet? Signup here!</a></p>
        </div>
        </form>;
    }
    if(this.props.auth.isLoaded && !this.props.auth.isEmpty){
        // auth is warmed up
        // and user is not logged in
		console.log(this.props);
        payload =
        
        <div>

	    <div>
	    	<header>
				<p id="title"><a href="/HomePage" style={{float:"left", marginLeft:400, fontSize:45, fontWeight: "bold"}}>My Career Timeline</a>

				
			</p>
			
			<p id="title">
			<Link to="/HomePage"><button variant="contained"
			
			    color="secondary" 
				style={{float:"left", fontWeight:"bold", fontSize:20, padding:"0 0 0 0"}}
			    onClick={() => {this.props.firebase.logout();}}>
			Logout
		    </button></Link>
				<Link to="/profile">My Profile</Link>
				<AnchorLink href='#card4'>How It Works</AnchorLink>
	    		<AnchorLink href='#card3'>Our Story</AnchorLink>
	    		<AnchorLink href='#card2'>Mission</AnchorLink>
		    	<AnchorLink href='#card1'>Demo</AnchorLink>
		    	</p>
				
	    	</header>
			
			
	    </div>

	    <div>
	    	<Grid container id="card1">
	    		<Grid item xl={3}>
	    			<img className="mountains" src="https://www.noahdigital.ca/wp-content/uploads/2018/01/our-mission-icon.png" />
	    		</Grid>
	    		<Grid item xs={1}>
	    		</Grid>
	    		<Grid item lg={6}>
	    			<h1 id="message">The smarter way to succeed.</h1>
	   				<p id="submessage">Create and organize timelines that auto-populate based on the goals you want to achieve.</p>
	   				<Link to="/signInteractives"><button className="demoButton">Try the Demo</button></Link> 
			
	    		</Grid>
	    	</Grid>
	   	</div>


	   	<div>
	   		<Grid container id="card2">
	   			<Grid item md={5}>
	   				<h1 id="message">Our Mission:</h1>
	   				<p id="submessage">Our mission is to provide career guidance and college access support for low-income, first generation high school students. We understand that the career planning process can be daunting, and many individuals may not have sufficient resources available to them. Our service aims to be the "helping hand" for those in need. Whether or not a person is sure of their ultimate goal, our objective stays the same: shed exposure on different career paths and provide useful reccomendations. Together, we can jumpstart the future entrepeneurs, athletes, and everything in between!</p>
	   			</Grid>
	   			<Grid item xl={6}>
	   				<img src="http://webinarmasterysummit.com/wp-content/uploads/2015/11/idea-rocket-e1449125760807.png"/>
	   			</Grid>
	   		</Grid>
	   	</div>

	   	<div>
	   		<Grid container id="card3">
	   			<Grid item xs={12}>
	   				<h1 id="message">Our Story:</h1>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="https://png.icons8.com/color/1600/person-female.png" id="icon"/>
	   				<p id="submessage"><b>Faria</b> completed her undergraduate in New York at NYU, where she started a college access organization called Project College which helped a little over a thousand underserved high school students in NYC apply for college and financial aid, and won the NYU Reynolds changemaker challenge first place award of $10,000 in 2014. Since then, She has been working at Intel in Finance for 3 years on the west coast (currently in San Jose, CA). She is currently pursuing her Masters in Data Science at UC Berkeley, with the goal of applying Data Science to social impact.</p>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Icons8_flat_businessman.svg/1024px-Icons8_flat_businessman.svg.png" id="icon"/>
					<p id="submessage"><b>Harsh</b> has a Masters degree in Computer Science, and about 6 years of experience in the field of software development. He is very passionate about the environment and social justice. Harsh is currently working at eBay in Seattle.</p>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="http://2017.igem.org/wiki/images/b/bd/AQAUnesp-team-icon.png" id="icon"/>
	   				<p id="submessage"><b>Amy, Austin, Erin, Sira, and Tara</b> are rising juniors, attending Cooper Union's Summer STEM program: Computer Science for Social Good. Undergoing the college application process themselves, they understand the stress it may entail. Together, with the help of Harsh and Faria, they intend to make a real difference and ease the pressure students face in finding their career plan.</p>
	   			</Grid>
	   			<Grid item xs={12}>
	   				<p id="submessage">Together we wanted to continue to work on the issue of college access and career guidance for underserved students, but using our technical skills to have more impact.</p>
	   			</Grid>
   			</Grid>
	   	</div>

	   	<div>
	   		<Grid container id="card4">
	   			
				<Grid item md={6}>
	   				<h1 id="message">How It Works:</h1>
	   				<p id="submessage">
					
					
					
					
				
					
					
					
					</p>
	   			</Grid>
				
				
				<Grid item xl={6}>
	   			</Grid>
				
				
	   		</Grid>
	   	</div>


	</div>
    }


    return(
	<div>
        {payload}
        </div>
        )
    }
};

//export default LoginPage
export default withStyles(styles)(compose(
    firebaseConnect(),
    connect(({firebase: {auth}}) => ({auth})),
    connect(({ firebase: { profile } }) => ({ profile }))
)(LoginPage));

