import React from 'react';
import TimelineEvent from './timeline-event.js';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {firebaseConnect, isLoaded, isEmpty} from "react-redux-firebase";
//import { Chart } from "react-google-charts";
import Grid from '@material-ui/core/Grid';

import Highcharts from 'highcharts';
import { HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, Legend, ColumnSeries } from 'react-jsx-highcharts';

import AnchorLink from 'react-anchor-link-smooth-scroll';
//this.props.end <--- that could be "doctor1"

class Results extends React.Component{

	
    render(){
    	//var start = this.props.start;
    	var title = this.props.goal.charAt(0).toUpperCase() + this.props.goal.slice(1);
    	var subtitle = this.props.goal.toLowerCase();
    	if (this.props.goal.charAt(0) == 'A' ||
    		this.props.goal.charAt(0) == 'E' ||
    		this.props.goal.charAt(0) == 'I' ||
    		this.props.goal.charAt(0) == 'O' ||
    		this.props.goal.charAt(0) == 'U' ||
    		this.props.goal.charAt(0) == 'Y'){
    		subtitle = 'an ' + subtitle;
    	}
    	else{
    		subtitle = 'a ' + this.props.goal;
    	}
    	
    	//var array = this.props.jobs[this.props.goal].events.slice(start);

    	let payload;

    	if(!isLoaded(this.props.jobs)){
    		payload = null;
    		console.log("PAYLOAD NULL");
    	}

    	/*if (isLoaded(this.props.events) &&
    		!isEmpty(this.props.events)){
    		payload = this.props.events.map(event =>{
    			return(
    				<TimelineEvent array={this.props.events}/>
    				);
    		});
    	}*/

    	if (isLoaded(this.props.jobs) &&
    		!isEmpty(this.props.jobs)){
    		console.log(this.props.jobs[this.props.goal].analysis.ug.cost);
    		console.log(this.props.jobs[this.props.goal].analysis.ug.salary);
    		console.log(this.props.jobs[this.props.goal].analysis.g.cost);
    		console.log(this.props.jobs[this.props.goal].analysis.g.salary);
    		let tuitionArray = [Number(this.props.jobs[this.props.goal].analysis.ug.cost),Number(this.props.jobs[this.props.goal].analysis.g.cost)];
    		let salaryArray = [Number(this.props.jobs[this.props.goal].analysis.ug.salary),Number(this.props.jobs[this.props.goal].analysis.g.salary)];
			console.log(tuitionArray);
    		payload = <div>
    					<Grid container>
    						<Grid item xs={12}>
	    						<p><b>Recommended Career Pathway</b></p>
	    						<TimelineEvent array={this.props.jobs[this.props.goal].events} />
	    					</Grid>
	    				</Grid>
	    					
	    				<Grid container>
	    					<Grid item xs={2}>
	    					</Grid>
	    					<Grid item xs={5}>
	    					{/*<Chart
	    						id = "chart"
	    						width = {'500px'}
	    						height = {'300px'}
	    						backgroundColor = 'rgb(0,0,0)'
	    						chartType = "Bar"
	    						loader = {<div>Loading Chart</div>}
	    						data = {[
	    							['Level of Education','Avg Tuition','Avg Salary'],
	    							['Undergrad',this.props.jobs[this.props.goal].analysis.ug.cost,this.props.jobs[this.props.goal].analysis.ug.salary],
	    							['Graduate',this.props.jobs[this.props.goal].analysis.g.cost,this.props.jobs[this.props.goal].analysis.g.salary],
	    		
	    						]}

	    						options = {{
	    							chart: {
	    								title: 'Cost-Benefit Analysis',
	    								subtitle: 'Average Tuition VS Average Annual Salary',
	    								
	    							},
	    							

	  								colors: ['#f94d53','#20d66f'],
	    							vAxis: {
	            						minValue: 0,
	           					  	 ticks: [0, 25000, 50000, 75000, 100000]
	          						}
	    							
	    						}}
	    					/>*/}
	    					
	    					<br/>
	    					<p><b>Cost-Benefit Analysis</b></p>
	    					
	    					<HighchartsChart>
	    						<Chart backgroundColor="#ffffff" height={300}/>
	    						
	    						<Legend/>
	    						<XAxis categories={['Undergraduate','Graduate']}/>
	    						<YAxis>
	    							<ColumnSeries name="Avg Tuition" data={tuitionArray} color="#f94d53"/>
	    							<ColumnSeries name="Avg Salary" data={salaryArray} color="#20d66f"/>
	    						</YAxis>
	    					</HighchartsChart>
	    					<br/>
	    					

	    					</Grid>

	    					<Grid item xs={3}>
	    						<p><b>Optimal Location:</b> {this.props.jobs[this.props.goal].city}</p>
    							<img id="locIcon" src="http://nichecc.com/wp-content/uploads/2018/01/places-api.png"/>
    						</Grid>
    					</Grid>


    				</div>;
    		console.log("Payload SUCCESS");
    	}

	return(
	    <div>
	    <Grid container>
	    <Grid item xs={1}>
	    </Grid>
	    <Grid item xs={10} id="results">
		<h1>{title}: Career Info</h1>
			<br/>
			{payload}
			<br/>
		<p>Like what you see? Sign up to start building your timeline around {subtitle} and add more reccomendations!</p>
	    <button className="demoButton">Sign Up</button>
	    </Grid>
	    <Grid item xs={1}>
	    </Grid>
	    </Grid>
	    </div>
	)
    }
}

export default withHighcharts(compose(
	firebaseConnect(props => [{ path: 'jobs'}]),
	connect((state, props) => ({
		jobs: state.firebase.data.jobs
	}))
	)(Results),Highcharts);
