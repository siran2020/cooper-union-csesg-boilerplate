import React from "react";

import Wheel_6 from "../components/Wheel.js";

import Wheel_9 from "../components/Wheel9.js";

import Wheel_4 from "../components/Wheel4.js";

import Wheel_2 from "../components/Wheel2.js";

import Wheel_3 from "../components/Wheel3.js";

import Wheel_5 from "../components/Wheel5.js";

import GoalQuestion from "../components/goal-question.js";

import Tiles from "../components/tiles.js";


class Allwheels extends React.Component{
	constructor(props){
		super(props);
		this.state = {};
	}

    render(){
    	var payload;

 

    	if(this.props.size == "wheel_4"){
    		payload = <Wheel_4 ops={this.props.ops} callbackFromParent={this.props.callbackFromParent} />
    		
    	}

		else if(this.props.size == "wheel_6"){

			payload = <Wheel_6 ops={this.props.ops} callbackFromParent={this.props.callbackFromParent} />

    	}

    	else if(this.props.size == "wheel_3"){

			payload = <Wheel_3 ops={this.props.ops} callbackFromParent={this.props.callbackFromParent} />

    	}



    	else if(this.props.size == "wheel_5"){

			payload = <Wheel_5 ops={this.props.ops} callbackFromParent={this.props.callbackFromParent} />

    	}


    	else if(this.props.size == "wheel_2"){

			payload = <Wheel_2 ops={this.props.ops} callbackFromParent={this.props.callbackFromParent} />

    	}


    	else if(this.props.size == "tiles") {
    		payload = <Tiles question={this.props.ops} callBack={this.props.callbackFromParent} />
    	}


	return(

	    <div>
		{payload}
	    </div>
	
    )
};
}

export default Allwheels
