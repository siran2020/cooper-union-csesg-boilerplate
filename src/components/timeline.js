
import React from "react";
import TimelineEvent from './timeline-event.js'

export default class TimeLine extends React.Component {
	/* constructor(props) {
		super(props);
		this.state = {
			events: {
				education: "highschool",
				title: "Hospital Internship"
			}
		};
	}

	*/

	render() {
		var events = [
			{
				title: "Freshman",
				desc: "First Year of HS"
			},



			{
				title: "College",
				desc: "Further Education"
			},


			{
				title: "Doctor",
				desc: "Work in the med field"
			}

		];

		// var eventItems = events.map(eventObject => {
		// 	return (
		// 		<TimelineEvent title={eventObject.title} desc = {eventObject.desc}/>
		// 	);
		// });

		return (
			<div>
				<p>DEMO TIMELINE FOR DOCTOR CAREER</p>
				<div className="bar" />
				<div className="timeline">

					<TimelineEvent array={events} />
					
				</div>
			</div>
		);
	}
}
