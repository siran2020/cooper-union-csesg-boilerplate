import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import LongMenu from '../components/LongMenu'
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Paper from '@material-ui/core/Paper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

const styles = {
  list: {
    width: 250,
  }
};

var a = "Summer Programs";
var b = "School Year Programs";
var c = "Courses";
var d = "Extracurriculars";
var array = [a, b, c, d];


var summerList = ["Summer1","Summer2","Summer3"];

class TemporaryDrawer extends React.Component {
	constructor(props){
    	super(props);
	};
  

  state = {
    array: [
        "Summer Programs",
        "School Year Programs",
        "Courses",
        "Extracurriculars",
    ],
    checkedA: false,
    left: false,
    
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };


  render() {
    const { classes } = this.props;

    const sideList = (
      <div>
        <List> 
        {/*this.state.array.map((element, i)=>{
          //console.log(element);
          <div>{element}</div>
        })*/}
        
        
          
        </List>
      </div>
    );

    const fullList = (
      // <div className=>
        <List></List>
      // </div>
    );

    return (
      <div>
        <Button onClick={this.toggleDrawer('left', true)}>Add to Timeline</Button>
       
        <SwipeableDrawer open={this.state.left} onClose={this.toggleDrawer('left', false)}>
          <div style={{width: 700 }}>
            <h1>Recommendations</h1>
          </div>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
          <LongMenu callbackFunction = {this.props.callbackFunction} name="Summer Programs"
                    array={["Baruch College STEM Program",
                            "Bronx Community College: An Introduction to Energy Technology",
                            "Brooklyn College Math Intensive",
                            "City College of New York Summer Architecture Program"]} />
          <LongMenu callbackFunction = {this.props.callbackFunction} name="School Year Programs"
                    array={["IBM Watson Internship",
                            "Girls Who Code",
                            "Envision National Youth Leadership Forum",
                            "City College Coding and Engineering Program"]} />
          <LongMenu callbackFunction = {this.props.callbackFunction} name="Courses"
                    array={["AP Computer Science",
                            "AP Calculus AB",
                            "AP Chemistry",
                            "Robotics"]} />
          <LongMenu callbackFunction = {this.props.callbackFunction} name="Extracurriculars"
                    array={["Hunter College Science Institute",
                            "New York City College of Technology Careers in Engineering and Computer Technologies",
                            "Queensborough Community College Biomedical Engineering",
                            "York College Fine Arts with Computer Graphics"]} />
        </SwipeableDrawer>
        
          
      </div>
    );
  }
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};


//export default withStyles(styles)(TemporaryDrawer); 
export default TemporaryDrawer

