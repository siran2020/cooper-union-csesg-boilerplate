import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class Tiles extends React.Component{
	constructor(props){
		super(props);
		this.state = {};
	}

    render(){

	return(

	    <div>

<h1 id="HEADER">{this.props.question.question}</h1>


			{this.props.question.answers.map((item, index) => (
					
					<button className="tile" onClick={() => {this.props.callBack(item.a, item.next)}}>
						{item.a}
					</button>
					
			))}

	    </div> 
	
    )
};
}

export default Tiles
