import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import registerServiceWorker from './registerServiceWorker';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
